+++
fragment = "contact"
#disabled = true
date = "2017-09-10"
weight = 1100
#background = "light"
form_name = "defaultContact"

title = "Contact"
subtitle  = ""

# PostURL can be used with backends such as mailout from caddy
post_url = "https://formspree.io/xjvakkqb" #default: formspree.io
#post_url = "https://formspree.io/xoqkvpea" #default: formspree.io
email = "pascale.garoux@gmail.com"
button = "Send Button" # defaults to theme default
#netlify = false

# Optional google captcha
#[recaptcha]
#  sitekey = ""

[message]
  success = "" # defaults to theme default
  error = "" # defaults to theme default

# Only defined fields are shown in contact form
[fields.name]
  text = "Your Name *"
  #error = "" # defaults to theme default

[fields.email]
  text = "Your Email *"
  #error = "" # defaults to theme default

#[fields.phone]
#  text = "Your Phone *"
#  #error = "" # defaults to theme default

[fields.message]
  text = "Your Message *"
  #error = "" # defaults to theme default

# Optional hidden form fields
# Fields "page" and "site" will be autofilled
[[fields.hidden]]
  name = "page"

[[fields.hidden]]
  name = "someID"
  value = "example.com"

[fields.contact]
  details = [
    ["fas fa-home", "Pascale Garoux, <br> 44 rue de Margnolles <br> Le Sémiramis - Allée 4 <br> 69300 Caluire-e    t-Cuire"],
    ["fas fa-phone", "+33 609 980 362"],
    ["fas fa-envelope", "pascale.garoux@gmail.com"],
  ]
#    ["fas fa-globe", '<a href="http://foiliumtranslation.com" target="_blank">foliumtranslation.com</a>']

+++
