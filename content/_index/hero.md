+++
fragment = "hero"
#disabled = true
date = "2016-09-07"
weight = 50
background = "dark" # can influence the text color
particles = true

title = "folium translation"
subtitle = "Pascale Garoux"
#explain = "Langues de travail : anglais et italien vers français.<br/>Traductions médicales et juridiques.<br/> Services de formation et de conseil linguistiques stratégiques. <br/> Relecture de documents et ouvrages rédigés en français."

explain = "Traduction médicale et juridique de l'anglais et l'italien vers le français.<br/>Relecture de documents et ouvrages rédigés en français.<br/>Formation et conseil linguistique stratégique.<br/>"

#[fields.explain]
[[details]]
  text = "Langues de travail : anglais et italien vers français."

[[details]]
  text = "Services de formation et de conseil linguistiques stratégiques."

[[details]]
  text = "Relecture de documents et ouvrages rédigés en français."

[header]
  #image = "header.jpg"
  image = "space.jpg"

[asset]
  image = "foliumtranslation.png"
  width = "256px" # optional - will default to image width
  #height = "150px" # optional - will default to image height

[[buttons]]
  text = "Découvrir mes services"
  url = "#items"
  color = "primary"

#[[buttons]]
#  text = "Traduction"
#  url = "#items"
#  color = "primary" # primary, secondary, success, danger, warning, info, light, dark, link - default: primary
#
#[[buttons]]
#  text = "Relecture"
#  url = "#items"
#  color = "primary"
#
#[[buttons]]
#  text = "Formation"
#  #url = "https://github.com/okkur/syna/releases"
#  url = "#items"
#  color = "primary"
#
#[[buttons]]
#  text = "Conseil"
#  url = "#items"
#  color = "primary"
+++
