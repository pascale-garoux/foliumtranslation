+++ 
title = "Traduction"
weight = 10
[asset]
  icon = "fas fa-language"
  #url = "#"
+++

</details>

**Mon expérience de professionnelle de la santé vouée à la traduction médicale**.

Je prends en charge **tous vos documents**, quelle que soit la spécialité, y compris les comptes-rendus médicaux, la médecine vétérinaire, dentaire, la dermatologie et la cosmétologie, le syndrome du pied diabétique, ainsi que le matériel, les dispositifs et consommables médicaux.

<!--
* dentaire et orthodontie
* dermatologie, cosmétologie et endermologie
* podologie, pied diabétique
* matériel, appareillages et consommables médicaux

Mes qualités de linguiste destinées au droit des contrats :
* contrats appliqués au domaine médical
* contrats de vente, de maintenance, d'agent commercial, de confidentialité, marchés publics, baux, RGPD
-->

**Mes qualités de linguiste destinées au droit des contrats et au droit médical** (consentement éclairé de patient, contrat d'essai clinique, PV d'accident de la route et du travail, cas de contentieux). 

