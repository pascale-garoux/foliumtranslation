+++ 
title = "Formation"
weight = 30
[asset]
  icon = "fas fa-graduation-cap"
  #url = "#"
+++

</details>

La traduction et la formation sont deux domaines indissociables, le principal objectif pour chacun d'eux étant de transmettre au mieux un message.

Mes formations s'adressent aux adultes professionnels et aux étudiants :
* FLE (français langue étrangère)
* grammaire comparée (anglais, italien, français)
* entraînement à l'expression écrite et orale des collaborateurs et salariés de votre entreprise
* conseils linguistiques
