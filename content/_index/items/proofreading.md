+++ 
title = "Relecture/Révision"
weight = 20
[asset]
  icon = "fas fa-book-reader"
  #url = "#"
+++

</details>

La révision s'intéresse à la stylistique, adapte lorsque nécessaire le contenu d'un texte, ligne après ligne. La relecture se consacre davantage à la vérification syntaxique, orthographique et typographique, dans le respect des conventions de rédaction. Elles sont intimement liées.

<!--
Types de documents traduits :
* médical : comptes-rendus, brochures, supports de formation, diaporama, essais cliniques, manuels et notices d'utilisation, lettres d'information
* juridique : contrats, contrats d'essai clinique, consentements libres et éclairés du patient, cas de contentieux médical, procès-verbaux d'accident de la route et du travail
* communiqués de presse et magazine d'entreprise
-->
