+++
fragment = "content"
#disabled = true
date = "2017-10-05"
weight = 100
#background = ""

title = "About Pascale Garoux"
#subtitle = ""
+++

Un parcours universitaire et professionnel n'est pas toujours linéaire. Il arrive parfois que la ligne que l'on s'était tracée dévie quelque peu, car, au fil du temps, les goûts se prononcent, évoluent, la maturité s'affirme, certaines pensées refont surface après avoir été minutieusement enfouies pour qu'enfin l'on s'émancipe de toute norme établie, que l'on fasse fi des conventions.

Après avoir exercé mon métier de podologue durant de nombreuses années, j'ai fait le choix d'une reconversion professionnelle, afin de me consacrer à la traduction spécialisée sous l'égide de Cosmic-Traduction. Alors, parallèlement à mon activité première, j'ai entrepris mes études de langues étrangères appliquées dans l'idée de créer un pont entre ces deux spécialités en apparence très opposées ; et pourtant, je retrouve ici la nécessité de ces compétences professionnelles que sont l'empathie, le questionnement et la réflexion.

Or, l'acte même de rédiger, propre à la traduction, ne se présente pas comme une évidence, même s'il existe au départ un attrait pour la lecture et les langues. Sont donc nécessaires à une telle action le temps et le discernement. Et bien plus encore, l'application. En effet, le traducteur est un véritable jardinier dont l'enthousiasme motive en toute saison sa curiosité quant à la richesse de la nature et la force symbolique qu’elle dégage. Chaque jour, il poursuit inlassablement son rituel, exécute sa tâche avec soin, cultive sans relâche et estime son dévouement indispensable. Il faut bien dire qu’un jardin respectable doit refléter la rigueur tant dans son agencement que son entretien, sa terre doit être sans cesse retournée afin qu’elle soit meuble et fertile, et ses plantations doivent être aimées, observées.

La demeure du traducteur est en tout point à l’image de ce jardin : l'essentiel, la clarté, l'exactitude et l'empressement rendent sa valeur inestimable. Aussi dans un contexte de communication internationale et d'externalisation des compétences la présence du traducteur s'avère-t-elle indispensable à la compréhension entre tous. Certaines formulations simples et fluides peuvent rendre les textes plus agréables à parcourir, moins rébarbatifs, donnant au lecteur concerné l'envie de s'informer. 

BUG
